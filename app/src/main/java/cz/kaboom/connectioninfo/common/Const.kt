package cz.kaboom.connectioninfo.common

object Const {
    const val LOG_TAG = "NILOG"
    const val SPEED_TEST_DOWNLOAD_URL = "ftp://speedtest.tele2.net/20MB.zip"
    const val SPEED_TEST_UPLOAD_URL = "ftp://speedtest.tele2.net/upload/"
    const val IPAPI_BASE_URL = "https://api.ipify.org"
    const val LOOKUP_BASE_URL = "http://ip-api.com/json/"
    const val UPLOAD_FILE_SIZE = 50 * 1000000
    const val EMPTY_STRING = ""
    const val SKIP_PACKETS_COUNT = 10
}