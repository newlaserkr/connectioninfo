package cz.kaboom.connectioninfo.api

import cz.kaboom.connectioninfo.dto.NetworkLookupDto
import retrofit2.Response
import retrofit2.http.*

interface NetworkLookupAPI {
    @GET("/")
    suspend fun getMyExternalIP(): Response<String>

    @GET("{ip_address}")
    suspend fun getLookupData(@Path(value = "ip_address") ip: String): Response<NetworkLookupDto>
}