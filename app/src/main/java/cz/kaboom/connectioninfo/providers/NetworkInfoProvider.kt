package cz.kaboom.connectioninfo.providers

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.wifi.WifiManager
import android.telephony.TelephonyManager
import android.util.Log
import androidx.lifecycle.LiveData
import cz.kaboom.connectioninfo.BuildConfig
import cz.kaboom.connectioninfo.R
import cz.kaboom.connectioninfo.api.NetworkLookupAPI
import cz.kaboom.connectioninfo.dto.NetworkLookupDto
import cz.kaboom.connectioninfo.common.Const
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Retrofit
import java.net.NetworkInterface
import java.util.*

enum class NetworkType {
    WIFI {
        override val asString = "WIFI"
    },
    CELLULAR {
        override val asString = "Cellular"
    },
    UNKNOWN {
        override val asString = "Unknown"
    };

    abstract val asString: String
}

class NetworkInformation {
    var isNetworkConnected: Boolean = false
    var internalIP: String = ""
    var externalIP: String? = ""
    var networkType: NetworkType = NetworkType.UNKNOWN
    var networkName: String? = ""
    var lookupData: NetworkLookupDto? = null
}

class NetworkInfoProvider(
    private val context: Context,
    private val ipService: Retrofit,
    private val lookupService: Retrofit
): LiveData<NetworkInformation>() {
    private var idle = true

    private val connectivityManager: ConnectivityManager
        get() = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    private val isNetworkConnected: Boolean
        get() {
            val activeNetwork: android.net.NetworkInfo? = connectivityManager.activeNetworkInfo
            return activeNetwork?.isConnectedOrConnecting == true
        }

    private val networkType: NetworkType
        get() {
            try {
                val network: Network? = connectivityManager.activeNetwork
                val cap: NetworkCapabilities? = connectivityManager.getNetworkCapabilities(network)

                cap ?: return NetworkType.UNKNOWN

                return when {
                    cap.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> NetworkType.CELLULAR
                    cap.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> NetworkType.WIFI
                    else -> NetworkType.UNKNOWN
                }
            } catch (e: Exception) {
                if (BuildConfig.DEBUG) Log.e(
                    Const.LOG_TAG,
                    "getNetworkType error: ${e.localizedMessage}"
                )
            }

            return NetworkType.UNKNOWN
        }

    private val networkName: String
        get() {
            return when (networkType) {
                NetworkType.CELLULAR -> (context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager).networkOperatorName
                NetworkType.WIFI -> (context.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager).connectionInfo.ssid
                NetworkType.UNKNOWN -> context.getString(R.string.unknown)
            }
        }

    private val internalIP: String
        get() {
            var result = "Unknown"
            try {
                for (itf in NetworkInterface.getNetworkInterfaces()) {
                    for (addr in itf.inetAddresses) {
                        if (!addr.isLoopbackAddress) {
                            result = addr.hostAddress.toUpperCase(Locale.getDefault())
                            if (result.indexOf('%') < 0) return result
                        }
                    }
                }
            } catch (e: Exception) {
                if (BuildConfig.DEBUG) Log.e(
                    Const.LOG_TAG,
                    "getInternalIP error: ${e.localizedMessage}"
                )
            }
            return result.substring(1, result.indexOf("%"))
        }


    suspend fun networkLookup() {
        fun isValid(ip: String?): Boolean = ip != null && ip != Const.EMPTY_STRING && ip.length > 8

        val result = NetworkInformation()

        if (!idle) return
        if (!this.isNetworkConnected) {
            postValue(result)
            return
        }
        idle = false

        try {
            result.isNetworkConnected = true
            result.internalIP = internalIP
            result.networkType = networkType
            result.networkName = networkName

            val ipResult = ipService.create(NetworkLookupAPI::class.java).getMyExternalIP()
            if (!ipResult.isSuccessful) throw Exception("Cannot get external IP address")
            result.externalIP = ipResult.body()
            if (!isValid(result.externalIP)) throw Exception("Cannot get external IP address")
            val infoResult = lookupService.create(NetworkLookupAPI::class.java).getLookupData(result.externalIP!!)
            if (!infoResult.isSuccessful) throw Exception("Cannot get Network information")
            result.lookupData = infoResult.body()
            withContext(Dispatchers.Main) { postValue(result) }
            idle = true

        } catch (e: Exception) {
            if (BuildConfig.DEBUG) Log.e(
                Const.LOG_TAG,
                "networkLookup error: ${e.localizedMessage}"
            )
        }
        idle = true
    }

}
