package cz.kaboom.connectioninfo.activity

import android.os.Bundle
import android.util.Log
import android.view.Window
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import cz.kaboom.connectioninfo.BuildConfig
import cz.kaboom.connectioninfo.R
import cz.kaboom.connectioninfo.common.Const
import cz.kaboom.connectioninfo.common.FloatCalcField
import cz.kaboom.connectioninfo.databinding.MainViewDataBinding
import cz.kaboom.connectioninfo.providers.NetworkInformation
import cz.kaboom.connectioninfo.providers.SpeedTestDirection
import cz.kaboom.connectioninfo.providers.SpeedTestEvent
import cz.kaboom.connectioninfo.providers.SpeedTestEvent.*
import cz.kaboom.connectioninfo.viewmodel.MainActivityViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.KoinComponent
import org.koin.core.get
import java.math.BigDecimal
import kotlin.math.roundToInt

fun <T> LifecycleOwner.observe(liveData: LiveData<T>, body: (T) -> Unit) {
    liveData.observe(this, Observer(body))
}

class MainActivity : AppCompatActivity(), KoinComponent {

    private val clcDownloadRate: FloatCalcField = get()
    private val clcUploadRate: FloatCalcField = get()
    private val mainActivityViewModel: MainActivityViewModel by viewModel()
    private lateinit var binding: MainViewDataBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        supportActionBar?.hide()
        actionBar?.hide()

        clcDownloadRate.formatString = getString(R.string.speed_format)
        clcUploadRate.formatString = getString(R.string.speed_format)

        binding = DataBindingUtil.setContentView(this, R.layout.page_view_layout)
        with(binding) {
            tvAppVersion.text = getString(R.string.version, packageManager.getPackageInfo(packageName, 0).versionName)
            idViewPager.pageTitles.add(getString(R.string.speed_test))
            idViewPager.pageTitles.add(getString(R.string.network_info))
            idTabLayout.setupWithViewPager(binding.idViewPager)
            pageSpeed.btnState.setOnClickListener { mainActivityViewModel.networkSpeedProvider.switchState() }
        }
        observe(mainActivityViewModel.internetStateProvider, ::onInternetStateChanged)
        observe(mainActivityViewModel.networkInfoProvider, ::onConnectionInfoReceived)
        observe(mainActivityViewModel.networkSpeedProvider, ::onSpeedTestEventReceived)
    }

    private fun onInternetStateChanged(isAvailable: Boolean) {
        if (isAvailable) {
            clearInfoFields()
            mainActivityViewModel.getNetworkInfo()
        }
    }

    private fun onConnectionInfoReceived(info: NetworkInformation) {
        with(binding.pageInfo) {
            tvNetworkType.text = info.networkType.asString
            tvInternalIp.text = info.internalIP
            tvExternalIp.text = info.externalIP
            tvProviderName.text = info.lookupData?.mISP
            tvOrganization.text = info.lookupData?.mOrg
            tvRegion.text = info.lookupData?.mRegion
            tvRegionName.text = info.lookupData?.mRegionName
            tvCountry.text = info.lookupData?.mCountry
            tvCountryCode.text = info.lookupData?.mCountryCode
            tvCity.text = info.lookupData?.mCity
            tvLatitude.text = info.lookupData?.mLat
            tvLongitude.text = info.lookupData?.mLon
        }
        binding.pageSpeed.btnState.isEnabled = true
        binding.pageSpeed.btnState.text = getString(R.string.start_test)
    }

    private fun onSpeedTestEventReceived(event: SpeedTestEvent) {
        when (event) {
            is State -> onSpeedTestState(event)
            is Data -> onSpeedTestData(event)
            is Error -> onSpeedTestError(event)
        }
    }

    private fun onSpeedTestState(event: State) {
        with(binding.pageSpeed) {
            btnState.isEnabled = !event.started
            btnState.text = if (event.started) getString(R.string.stop_test) else getString(R.string.start_test)
            if (!event.started) clearSpeedFields()
        }
    }

    private fun onSpeedTestData(event: Data) {
        with(binding.pageSpeed) {
            if (!btnState.isEnabled) btnState.isEnabled = true

            speedTestGauge.currentValue = event.bitsPerSec.toFloat() / 1000000f
            when (event.direction) {
                SpeedTestDirection.DOWNLOAD -> {
                    clcDownloadRate.set((event.bitsPerSec / BigDecimal(1000000)).toFloat())
                    tvProgress.text = getString(R.string.dl_progress_format, event.percent)
                    progressBar.progress = event.percent.roundToInt()
                    tvDownloadCurrent.text = clcDownloadRate.current
                    tvDownloadMax.text = clcDownloadRate.maximum
                    tvDownloadAvg.text = clcDownloadRate.average
                }
                SpeedTestDirection.UPLOAD -> {
                    clcUploadRate.set((event.bitsPerSec / BigDecimal(1000000)).toFloat())
                    tvProgress.text = getString(R.string.ul_progress_format, event.percent)
                    progressBar.progress = event.percent.roundToInt()
                    tvUploadCurrent.text = clcUploadRate.current
                    tvUploadMax.text = clcUploadRate.maximum
                    tvUploadAvg.text = clcUploadRate.average
                }
                else -> null
            }
        }
    }

    private fun onSpeedTestError(event: Error) {
        if (BuildConfig.DEBUG) Log.e(
            Const.LOG_TAG,
            "onSpeedTestError: ${event.error.localizedMessage}"
        )
        binding.pageSpeed.btnState.isEnabled = true
        binding.pageSpeed.btnState.text = getString(R.string.start_test)

    }

    private fun clearInfoFields() {
        with(binding.pageInfo) {
            tvNetworkType.text = Const.EMPTY_STRING
            tvInternalIp.text = Const.EMPTY_STRING
            tvExternalIp.text = Const.EMPTY_STRING
            tvProviderName.text = Const.EMPTY_STRING
            tvOrganization.text = Const.EMPTY_STRING
            tvRegion.text = Const.EMPTY_STRING
            tvRegionName.text = Const.EMPTY_STRING
            tvCountry.text = Const.EMPTY_STRING
            tvCountryCode.text = Const.EMPTY_STRING
            tvLatitude.text = Const.EMPTY_STRING
            tvLongitude.text = Const.EMPTY_STRING
        }
    }

    private fun clearSpeedFields() {
        with(binding.pageSpeed) {
            tvProgress.text = Const.EMPTY_STRING
            progressBar.progress = 0
            speedTestGauge.currentValue = 0f
        }
    }
}
