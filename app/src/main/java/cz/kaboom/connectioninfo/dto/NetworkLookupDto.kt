package cz.kaboom.connectioninfo.dto

import com.google.gson.annotations.SerializedName
import cz.kaboom.connectioninfo.common.Const

data class NetworkLookupDto
    (
    @SerializedName("as") var mAs: String = Const.EMPTY_STRING,
    @SerializedName("city") var mCity: String = Const.EMPTY_STRING,
    @SerializedName("country") var mCountry: String = Const.EMPTY_STRING,
    @SerializedName("countryCode") var mCountryCode: String = Const.EMPTY_STRING,
    @SerializedName("isp") var mISP: String = Const.EMPTY_STRING,
    @SerializedName("lat") var mLat: String = Const.EMPTY_STRING,
    @SerializedName("lon") var mLon: String = Const.EMPTY_STRING,
    @SerializedName("org") var mOrg: String = Const.EMPTY_STRING,
    @SerializedName("query") var mQuery: String = Const.EMPTY_STRING,
    @SerializedName("region") var mRegion: String = Const.EMPTY_STRING,
    @SerializedName("regionName") var mRegionName: String = Const.EMPTY_STRING,
    @SerializedName("status") var mStatus: String = Const.EMPTY_STRING,
    @SerializedName("timezone") var mTimezone: String = Const.EMPTY_STRING,
    @SerializedName("zip") var mZip: String = Const.EMPTY_STRING
)
